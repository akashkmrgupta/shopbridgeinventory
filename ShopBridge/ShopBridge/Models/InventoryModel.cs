﻿using System;

namespace ShopBridge.Models
{
    public class InventoryModel
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string Path { get; set; }
        public string base64Image { get; set; }
        public byte[] ByteImage { get; set; }
        public byte[] Thumbnail { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsActive { get; set; }
    }
}

