using ShopBridge.Models;
using ShopBridge.Repository;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ShopBridge.Controllers
{
    /// <summary>
    /// This will create transport with given parameters
    /// </summary>
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ProductInventoryController : ApiController
    {
        private IProductRepository _productRepository;

        public ProductInventoryController()
        {
            _productRepository = new ProductRepository(new ShopBridgeDBEntities());
        }

        private ProductInventoryController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        /// <summary>
        /// This will get all active Product
        /// </summary>
        [HttpGet]
        public async Task<IHttpActionResult> GetAllActiveProducts()
        {
            var products = _productRepository.GetAll();

            if (products == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        /// <summary>
        /// This will get Product by id
        /// </summary>
        [HttpGet]
        public async Task<IHttpActionResult> GetProductById(Guid Id)
        {
            var product = _productRepository.GetById(Id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        /// <summary>
        /// This will create or update Product with given parameters
        /// </summary>
        [HttpPost]
        public async Task<IHttpActionResult> InsertUpdateProduct(InventoryModel product)
        {
            var result = _productRepository.InsertOrUpdate(product);
            if (result == 1)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// This will delete product with given id
        /// </summary>
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductById(Guid id)
        {
            _productRepository.Delete(id);

            return Ok();
        }
    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    