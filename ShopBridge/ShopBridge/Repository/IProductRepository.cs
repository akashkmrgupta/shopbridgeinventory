﻿using ShopBridge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBridge.Repository
{
    interface IProductRepository
    {
        IEnumerable<InventoryModel> GetAll();
        InventoryModel GetById(Guid Id);
        int InsertOrUpdate(InventoryModel Product);
        void Delete(Guid ProductID);
    }
}
