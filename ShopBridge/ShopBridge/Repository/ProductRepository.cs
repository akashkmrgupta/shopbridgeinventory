﻿using System;
using System.Collections.Generic;
using System.Linq;
using ShopBridge.Models;

namespace ShopBridge.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ShopBridgeDBEntities _context;

        public ProductRepository()
        {
            _context = new ShopBridgeDBEntities();
        }

        public ProductRepository(ShopBridgeDBEntities context)
        {
            _context = context;
        }

        public IEnumerable<InventoryModel> GetAll()
        {
            List<InventoryModel> products = new List<InventoryModel>();

            using (var ctx = new ShopBridgeDBEntities())
            {
                var resproduct = ctx.td_productinventory.Where(p => p.isactive == true);

                foreach (var item in resproduct)
                {
                    products.Add(new InventoryModel
                    {
                        ProductId = item.productid,
                        Name = item.name,
                        Description = item.description,
                        Price = item.price,
                        base64Image = item.base64Image,
                        CreatedOn = item.createdon,
                        ModifiedOn = item.modifiedon,
                        IsActive = item.isactive,
                    });
                }
            }
            return products.OrderByDescending(p => p.ModifiedOn);
        }

        public InventoryModel GetById(Guid ProductId)
        {
            InventoryModel product = new InventoryModel();

            using (var ctx = new ShopBridgeDBEntities())
            {
                var resProduct = ctx.td_productinventory.Where(p => p.productid == ProductId && p.isactive == true)
                    .FirstOrDefault();

                product.CreatedOn = resProduct.createdon;
                product.Description = resProduct.description;
                product.IsActive = resProduct.isactive;
                product.ModifiedOn = resProduct.modifiedon;
                product.Name = resProduct.name;
                product.Price = resProduct.price;
                product.ProductId = resProduct.productid;
                product.base64Image = resProduct.base64Image;
            }
            return product;
        }

        public int InsertOrUpdate(InventoryModel Product)
        {
            using (var ctx = new ShopBridgeDBEntities())
            {
                if (Product.ProductId != Guid.Empty)
                {
                    var existingProduct = ctx.td_productinventory.Where(p => p.productid == Product.ProductId)
                                                    .FirstOrDefault<td_productinventory>();

                    //update details
                    existingProduct.name = Product.Name;
                    existingProduct.description = Product.Description;
                    existingProduct.price = Product.Price;
                    if (Product.base64Image!=null)
                    {
                        existingProduct.base64Image = Product.base64Image;
                    }
                    existingProduct.modifiedon = DateTime.Now;
                    ctx.SaveChanges();
                }
                else
                {
                    var newProductId = Guid.NewGuid();

                    //insert details
                    ctx.td_productinventory.Add(new td_productinventory()
                    {
                        productid = newProductId,
                        name = Product.Name,
                        description = Product.Description,
                        price = Product.Price,
                        createdon = DateTime.Now,
                        modifiedon = DateTime.Now,
                        isactive = true,
                        base64Image = Product.base64Image
                    });
                    ctx.SaveChanges();
                }
            }
            return 1;
        }

        public void Delete(Guid ProductID)
        {
            using (var ctx = new ShopBridgeDBEntities())
            {
                var existingProduct = ctx.td_productinventory.Where(p => p.productid == ProductID)
                                                    .FirstOrDefault<td_productinventory>();

                existingProduct.isactive = false;
                ctx.SaveChanges();
            }
        }
    }
}